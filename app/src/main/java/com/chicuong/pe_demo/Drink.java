package com.chicuong.pe_demo;

import java.io.Serializable;
import java.sql.Time;

public class Drink implements Serializable {
    private int DrinkID;
    private String DrinkName;
    private float Price;
    private boolean Status;
    private String TimeOfCreate;

    public Drink(int drinkID, String drinkName, float price, boolean status, String timeOfCreate) {
        DrinkID = drinkID;
        DrinkName = drinkName;
        Price = price;
        Status = status;
        TimeOfCreate = timeOfCreate;
    }

    public int getDrinkID() {
        return DrinkID;
    }

    public void setDrinkID(int drinkID) {
        DrinkID = drinkID;
    }

    public String getDrinkName() {
        return DrinkName;
    }

    public void setDrinkName(String drinkName) {
        DrinkName = drinkName;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float price) {
        Price = price;
    }

    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public String getTimeOfCreate() {
        return TimeOfCreate;
    }

    public void setTimeOfCreate(String timeOfCreate) {
        TimeOfCreate = timeOfCreate;
    }

    @Override
    public String toString() {
        return DrinkID + "," + DrinkName + "," + Price + ","
                + Status + "," + TimeOfCreate;
    }
}
