package com.chicuong.pe_demo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class DrinkAdapter extends BaseAdapter {

    private List<Drink> list;
    private LayoutInflater inflater;
    private Context context;

    public DrinkAdapter(List<Drink> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list.get(i).getDrinkID();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null)  {
            view = inflater.inflate(R.layout.drink_grid_item, viewGroup, false);
        }
        TextView txtID = view.findViewById(R.id.txtId);
        TextView txtName = view.findViewById(R.id.txtName);
        TextView txtPrice = view.findViewById(R.id.txtPrice);
        TextView txtStatus = view.findViewById(R.id.txtStatus);

        Drink drink = this.list.get(i);

        txtID.setText(String.valueOf(drink.getDrinkID()));
        txtName.setText(drink.getDrinkName());
        txtPrice.setText(String.valueOf(drink.getPrice()));
        txtStatus.setText(String.valueOf(drink.isStatus()));
        return view;
    }

    public void setList(List<Drink> list) {
        this.list = list;
    }
}
